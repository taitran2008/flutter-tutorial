import 'package:flutter/material.dart';
import 'package:flutter_app/animation/screen/home.dart';

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return MaterialApp(
        title: 'Animation',
        theme: ThemeData(primarySwatch: Colors.blue),
        home: Home());
  }
}
