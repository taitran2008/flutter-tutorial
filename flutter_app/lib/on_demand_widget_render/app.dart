import 'package:flutter/material.dart';
import 'package:flutter_app/on_demand_widget_render/bloc/comments_provider.dart';
import 'package:flutter_app/on_demand_widget_render/bloc/stories_provider.dart';
import 'package:flutter_app/on_demand_widget_render/screens/news_detail.dart';
import 'package:flutter_app/on_demand_widget_render/screens/news_list.dart';

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return CommentsProvider(
      child: StoriesProvider(
        child: MaterialApp(
          title: 'News !',
          onGenerateRoute: routes,
        ),
      ),
    );
  }

  Route routes(RouteSettings settings) {
    if (settings.name == '/') {
      return MaterialPageRoute(
          builder: (context) {
            final storiesBloc = StoriesProvider.of(context);
            storiesBloc.fetchTopIds();
            return NewsList();
          }
      );
    }
    else {
      return MaterialPageRoute(
          builder: (context) {
            //extract the item id from settings.name
            final commentBloc = CommentsProvider.of(context);
            final itemId = int.parse(settings.name.replaceFirst('/', ''));
            commentBloc.fetchItemWithComments(itemId);
            return NewsDetail(
                itemId: itemId
            );
          }
      );
    }
  }
}
