import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_app/data_fetching/models/item_models.dart';
import 'package:flutter_app/on_demand_widget_render/bloc/stories_provider.dart';
import 'package:flutter_app/on_demand_widget_render/widgets/loading_container.dart';

class NewListTile extends StatelessWidget {
  final int itemId;

  NewListTile(this.itemId);

  @override
  Widget build(BuildContext context) {
    final bloc = StoriesProvider.of(context);
    // TODO: implement build
    return StreamBuilder(
      stream: bloc.items,
      builder: (context, AsyncSnapshot<Map<int, Future<ItemModel>>> snapshot) {
        if (!snapshot.hasData) {
          return LoadingContainer();
        }
        return FutureBuilder(
          future: snapshot.data[itemId],
          builder: (context, AsyncSnapshot<ItemModel> itemSnapshot) {
            if (!itemSnapshot.hasData) {
              return LoadingContainer();
            }
            return buildTile(context, itemSnapshot.data);
          },
        );
      },
    );
  }

  Widget buildTile(BuildContext context, ItemModel item) {
    return Column(
      children: <Widget>[
        ListTile(
          title: Text(item.title),
          onTap: () {
            Navigator.pushNamed(context, '/${item.id}');
          },
          subtitle: Text('${item.score} points'),
          trailing: Column(
            children: <Widget>[
              Icon(Icons.comment),
              Text('${item.descendants}')
            ],
          ),
        ),
        Divider(
          height: 4.0,
        )
      ],
    );
  }
}
