import 'package:flutter/material.dart';
import 'package:flutter_app/on_demand_widget_render/bloc/stories_provider.dart';
import 'package:flutter_app/on_demand_widget_render/widgets/news_list_tile.dart';
import 'package:flutter_app/on_demand_widget_render/widgets/refresh.dart';

class NewsList extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final bloc = StoriesProvider.of(context);
    //Very bad here

    // TODO: implement build
    return Scaffold(
      appBar: AppBar(
        title: Text('Top News'),
      ),
      body: buildList(bloc),
    );
  }

  Widget buildList(StoriesBloc bloc) {
    return StreamBuilder(
      stream: bloc.topIds,
      builder: (context, AsyncSnapshot<List<int>> snapshot) {
        if (!snapshot.hasData) {
          return Center(
            child: CircularProgressIndicator(),
          );
        }

        return Refresh(
          child: ListView.builder(
              itemCount: snapshot.data.length,
              itemBuilder: (context, int index) {
                bloc.fetchItem(snapshot.data[index]);
                return NewListTile(snapshot.data[index]);
              }),
        );
      },
    );
  }
}
