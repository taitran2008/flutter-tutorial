import 'dart:async';

import 'package:flutter_app/data_fetching/models/item_models.dart';
import 'package:flutter_app/data_fetching/resources/repository.dart';
import 'package:rxdart/rxdart.dart';

class CommentsBloc {
  final _commentsFetcher = PublishSubject<int>();
  final _commentsOutput = BehaviorSubject<Map<int, Future<ItemModel>>>();
  final _repository = Repository();

  //Streams
  Observable<Map<int, Future<ItemModel>>> get itemsWithComments =>
      _commentsOutput.stream;

  dispose() {
    _commentsFetcher.close();
    _commentsOutput.close();
  }

  CommentsBloc() {
    _commentsFetcher.stream
        .transform(_commnentsTransformer())
        .pipe(_commentsOutput);
  }

  _commnentsTransformer() {
    return ScanStreamTransformer<int, Map<int, Future<ItemModel>>>(
      (cache, id, index) {
        print(index);
        cache[id] = _repository.fetchItem(id);
        cache[id].then((ItemModel itemModel) {
          itemModel.kids.forEach((kidId) => fetchItemWithComments(kidId));
        });
        return cache;
      },
      <int, Future<ItemModel>>{},
    );
  }

  Function(int) get fetchItemWithComments => _commentsFetcher.sink.add;
}
