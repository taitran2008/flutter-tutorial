import 'package:flutter/material.dart';
import 'screens/LoginScreen.dart';
import 'package:flutter_app/bloc_login/bloc/Provider.dart';

class MyApp extends StatelessWidget{
  @override
  Widget build(BuildContext context) {
    return Provider(
      child: MaterialApp(
        title: 'Log me in',
        home: Scaffold(
          body: LoginScreen(),
        ),
      )
    );
  }
}