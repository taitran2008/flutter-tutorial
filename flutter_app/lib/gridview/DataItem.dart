import 'package:flutter/cupertino.dart';

class DataItem{
  final int index;
  final String name;
  final String images;

  DataItem({
      @required this.index,
      @required this.name,
      @required this.images});


}