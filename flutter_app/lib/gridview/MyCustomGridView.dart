import 'package:flutter/material.dart';
import 'package:flutter_app/gridview/DataItem.dart';

class MyCustomGridView extends StatefulWidget{

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return GridViewState();
  }

  MyCustomGridView();

}
class GridViewState extends State<MyCustomGridView>{

  Function get addItemFunc => _addGridItem;

  List<DataItem> items = [DataItem(
      index: 1,
      images: 'assets/social/twitter.png',
      name: 'Twitter'
  ),
    DataItem(
        index: 2,
        images: 'assets/social/linkedin.png',
        name: 'LinkedIn'
    ),
    DataItem(
        index: 3,
        images: 'assets/social/google_plus.png',
        name: 'Google Plus'
    ),
    DataItem(
        index: 4,
        images: 'assets/social/ic_launcher.png',
        name: 'IC Launcher'
    ),
    DataItem(
        index: 5,
        images: 'assets/social/instagram.png',
        name: 'Instagram'
    ),
    DataItem(
        index: 6,
        images: 'assets/social/facebook.png',
        name: 'Facebook'
    )
    ,
    DataItem(
        index: 7,
        images: 'assets/social/linkedin.png',
        name: 'LinkedIn'
    ),
    DataItem(
        index: 8,
        images: 'assets/social/google_plus.png',
        name: 'Google Plus'
    ),
    DataItem(
        index: 9,
        images: 'assets/social/ic_launcher.png',
        name: 'IC Launcher'
    ),
    DataItem(
        index: 10,
        images: 'assets/social/instagram.png',
        name: 'Instagram'
    ),
    DataItem(
        index: 11,
        images: 'assets/social/facebook.png',
        name: 'Facebook'
    )
  ];

  GridViewState();

  _addGridItem(){
    setState(() {
      items.add(DataItem(
        index: 69,
        name: 'this is it',
        images: 'assets/social/ic_launcher.png'
      ));
    });
  }


  @override
  Widget build(BuildContext context) {

    // TODO: implement build
    return Container(
        color: Colors.blue,
        child: GridView.builder(
            itemCount : items.length,
            gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                crossAxisCount: 2
            ),
            itemBuilder: (context, position){
              return GestureDetector(
                onTap: (){
                  print('OnTap '+items[position].name);
                },
                child: Card(
                  elevation: 1.5,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    mainAxisSize: MainAxisSize.min,
                    verticalDirection: VerticalDirection.down,
                    children: <Widget>[
                      Center(
                        child: Text(items[position].name),
                      ),
                      Expanded(
                          child:Image(image: AssetImage(items[position].images),)
                      )
                    ],
                  ),
                ),
              );
            })

    );
  }
}

