import 'package:flutter/material.dart';

class MyGridView{
  GestureDetector getStructuredGridCell(name, image){
    return GestureDetector(
          onTap: (){
            print('OnTap called');
          },
          child: Card(
            elevation: 1.5,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              mainAxisSize: MainAxisSize.min,
              verticalDirection: VerticalDirection.down,
              children: <Widget>[
                Image(image: AssetImage('assets/social/'+image)),
                Center(
                  child: Text(name),
                )
              ],
            ),
          ),
    )
    ;
  }



  GridView build(){
    return GridView.count(
      crossAxisCount: 2,
      primary: true,
      padding: const EdgeInsets.all(1.0),
      childAspectRatio: 0.85,
      mainAxisSpacing: 1.0,
      crossAxisSpacing: 1.0,
      children: <Widget>[
        getStructuredGridCell("Facebook", "facebook.png"),
        getStructuredGridCell("Twitter", "twitter.png"),
        getStructuredGridCell("Instagram", "instagram.png"),
        getStructuredGridCell("Linkedin", "linkedin.png"),
        getStructuredGridCell("Google Plus", "google_plus.png"),
        getStructuredGridCell("Laucher Icon", "ic_launcher.png"),
      ],);

  }
}

