import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' show get;
import 'ImageModel.dart';
import 'ImageList.dart';

class MyApp extends StatefulWidget{
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return MyAppSate();
  }
}

class MyAppSate extends State<MyApp>{
  int counter = 0;
  List<ImageModel> images = [];

  fetchImage() async{
      counter++;
      var response = await get('https://jsonplaceholder.typicode.com/photos/$counter');
      var imageModel = ImageModel.fromJson(json.decode(response.body));
      setState(() {
        images.add(imageModel);
      });
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return MaterialApp(
      home: Scaffold(
        body: ImageList(images),
        floatingActionButton: FloatingActionButton(
          child: Icon(Icons.add),
          onPressed: (){
            fetchImage();
          },
        ),
        appBar: AppBar(
          title: Text('Some images in here !'),
        ),
      ),
    );
  }


}