import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app/http/ImageModel.dart';

class ImageList extends StatelessWidget{

  final List<ImageModel> images;

  ImageList(this.images);

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return ListView.builder(
        itemCount:images.length,
        itemBuilder: (context, index){
          return buildImage(images[index]);
        },
    );
  }

  Widget buildImage(ImageModel image){
    return Container(
      margin: EdgeInsets.only(left :20.0, top : 20.0, right : 20.0, bottom : 0.1),
      child: Column(
        children: <Widget>[
          Image.network(image.url),
          SizedBox(
            height: 10,
          ),
          Text(image.title)
        ],
      ),
      padding: EdgeInsets.all(20.0),
      decoration: BoxDecoration(
          border: Border.all(color: Colors.green, width: 1, style: BorderStyle.solid)
      ),
    );
  }
}