import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:redux/redux.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'Middleware.dart';
import 'Reducers.dart';
import 'State.dart';
import 'ToDoListPage.dart';
import 'ToDoItem.dart';


class ToDolistApp extends StatelessWidget{
  final Store<AppState> store = Store<AppState>(
    appReducer,
    initialState: AppState.initial(),
    middleware: createStoreMiddleWare()
  );

  @override
  Widget build(BuildContext context) => StoreProvider(
    store: this.store,
    child: MaterialApp(
      title: 'Flutter Redux Demo',
      theme: ThemeData(
        primarySwatch: Colors.teal
      ),
      home: ToDoListPage(),
    ),
  );

}