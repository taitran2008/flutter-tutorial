import 'package:flutter_app/redux1/ToDoItem.dart';

class AppState {
  final List<ToDoItem> toDos;
  final ListState listState;

  AppState(this.toDos, this.listState);

  initial(){
    print('initial');
  }


//https://stackoverflow.com/questions/52299304/dart-advantage-of-a-factory-constructor-identifier
  factory AppState.initial() => AppState(List.unmodifiable([]), ListState.listOnly);
}


enum ListState{
  listOnly, listWithNewItem
}


