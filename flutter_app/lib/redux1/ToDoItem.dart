class ToDoItem{
   final String title;

   ToDoItem(this.title);

   @override
  bool operator ==(other) {
    // TODO: implement ==
    return
      identical(this, other)
          || other is ToDoItem && runtimeType == other.runtimeType && title == other.title;
  }

  @override
  // TODO: implement hashCode
  int get hashCode => title.hashCode;

}