import 'dart:async';

import 'package:flutter_app/redux1/State.dart';
import 'package:redux/redux.dart';
import 'package:flutter_app/redux1/Action.dart';

List<Middleware<AppState>> createStoreMiddleWare() => [
  TypedMiddleware<AppState, SaveListAction>(_saveList),
];

Future _saveList(Store<AppState> store, SaveListAction action, NextDispatcher next) async{
  await Future.sync(()=>Duration(seconds: 3));
  next(action);
}