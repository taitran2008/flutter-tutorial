import 'dart:async';
import 'dart:io';

import 'package:flutter_app/data_fetching/models/item_models.dart';
import 'package:path/path.dart';
import 'package:path_provider/path_provider.dart';
import 'package:sqflite/sqflite.dart';

import 'repository.dart';

class NewsDBProvider implements Source, Cache {
  Database db;

  NewsDBProvider() {
    init();
  }

  init() async {
    Directory documentDirectory = await getApplicationDocumentsDirectory();
    final path = join(documentDirectory.path, "items.db");
    db = await openDatabase(
      path,
      version: 1,
      onCreate: (Database newDb, int version) {
        newDb.execute("""
            CREATE TABLE Items
            (
              id INTEGER PRIMARY KEY,
              type TEXT,
              by TEXT,
              time INTEGER,
              text TEXT,
              parent INTEGER,
              kids BLOB,
              dead INTEGER,
              deleted INTEGER,
              url TEXT,
              score INTEGER,
              title TEXT,
              descendants INTEGER
            )
          """);
      },

    );
  }

  Future<ItemModel> fetchItem(int id) async {
    final map = await db.query(
      "Items",
      columns: null,
      where: "id = ?",
      whereArgs: [id],
    );

    if (map.length > 0) {
      return ItemModel.fromDb(map.first);
    }

    return null;
  }

  //no need async, await ?? dont care return
  Future<int> addItem(ItemModel item) {
    return db.insert("Items",
        item.toMap(),
        conflictAlgorithm: ConflictAlgorithm.ignore);
  }

  @override
  Future<List<int>> fetchTopIds() {
    // TODO: implement fetchTopIds
    return null;
  }

  Future<int> clear() {
    return db.delete('Items');
  }
}

final newDBProvider = NewsDBProvider();