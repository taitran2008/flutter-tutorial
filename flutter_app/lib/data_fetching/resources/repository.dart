import 'dart:async';

import 'package:flutter_app/data_fetching/models/item_models.dart';

import 'new_db_provider.dart';
import 'news_api_provider.dart';

class Repository {
  List<Source> sources = <Source>[
    newDBProvider,
    newsApiProvider,
  ];

  List<Cache> caches = <Cache>[newDBProvider];

  NewsDBProvider dbProvider = newDBProvider;
  NewsApiProvider apiProvider = newsApiProvider;

  Future<List<int>> fetchTopIds() async {
    return apiProvider.fetchTopIds();
  }

  Future<ItemModel> fetchItem(int id) async {
    ItemModel item;
    var source;

    for (source in sources) {
      item = await source.fetchItem(id);
      if (item != null) {
        break;
      }
    }

    for (var cache in caches) {
      if (cache != source) {
        cache.addItem(item);
      }
    }
    return item;
  }

  clearCache() async {
    for (var cache in caches) {
      await cache.clear();
    }
  }
}


abstract class Source {
  Future<List<int>> fetchTopIds();

  Future<ItemModel> fetchItem(int id);
}

abstract class Cache {
  Future<int> addItem(ItemModel item);

  Future<int> clear();
}