import 'package:flutter/material.dart';
import 'package:flutter_app/login_stateful/screens/LoginScreen.dart';

class MyApp extends StatelessWidget{


  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return MaterialApp(
      title: 'Login form',
      home: Scaffold(
        body: LoginScreen(),
      ),
    );
  }


}